<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Endereco extends Model
{
    use HasFactory;

    protected $table = 'endereco';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userid',
        'rua',
        'numero',
        'complemento',
        'bairro',
        'cidade',
        'estado',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];


    public function users(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
